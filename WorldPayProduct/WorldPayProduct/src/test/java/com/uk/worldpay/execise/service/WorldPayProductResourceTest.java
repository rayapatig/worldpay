package com.uk.worldpay.execise.service;

import com.uk.worldpay.exercise.model.Message;
import com.uk.worldpay.exercise.model.Product;
import com.uk.worldpay.exercise.resources.WorldPayProductResource;
import com.uk.worldpay.exercise.services.WorldPayProductService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpEntity;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class WorldPayProductResourceTest {

    @Mock
    private WorldPayProductService worldPayProductServiceMock;

    @InjectMocks
    private WorldPayProductResource restResource;

    @Before
    public void setup() {
        Mockito.reset(worldPayProductServiceMock);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testWhenCreateProductOffer() {
        Product product = createProduct();

        HttpEntity<Message> message = restResource.createProductOffer(product);
        verify(worldPayProductServiceMock, times(1)).saveProductOffer(any(Product.class));
        Assert.assertEquals("product offer has been successfully saved", message.getBody().getMessage());
    }

    @Test
    public void testWhenGetAllProductOffers() {
        Product product = createProduct();
        List<Product> productList = new ArrayList<>();
        productList.add(createProduct());
        productList.add(createProduct());
        productList.add(createProduct());
        HttpEntity<Message> message = restResource.createProductOffer(product);

        verify(worldPayProductServiceMock, times(1)).saveProductOffer(any(Product.class));
        Assert.assertEquals("product offer has been successfully saved", message.getBody().getMessage());
        when(worldPayProductServiceMock.getAllProducts()).thenReturn(productList);
        List<Product> productOffersList = restResource.getAllProducts();

        Assert.assertEquals(3, productOffersList.size());
        verify(worldPayProductServiceMock, times(1)).getAllProducts();

    }

    @Test
    public void testWhenGetCancelledProductOffers() {
        Product product = createProduct();
        product.setCancelled(Boolean.TRUE);
        List<Product> productList = new ArrayList<>();
        productList.add(product);
        productList.add(product);
        productList.add(createProduct());
        HttpEntity<Message> message = restResource.createProductOffer(product);

        verify(worldPayProductServiceMock, times(1)).saveProductOffer(any(Product.class));
        Assert.assertEquals("product offer has been successfully saved", message.getBody().getMessage());
        when(worldPayProductServiceMock.getCancelledProducts()).thenReturn(productList);
        List<Product> cancelledProductsList = restResource.getCancelledProducts();
        Assert.assertNotNull(cancelledProductsList);
        verify(worldPayProductServiceMock, times(1)).getCancelledProducts();

    }

    @Test
    public void testWhenGetValidProductOffers() {
        Product product = createProduct();
        List<Product> productList = new ArrayList<>();
        productList.add(createProduct());
        productList.add(createProduct());
        productList.add(createProduct());
        HttpEntity<Message> message = restResource.createProductOffer(product);

        verify(worldPayProductServiceMock, times(1)).saveProductOffer(any(Product.class));
        Assert.assertEquals("product offer has been successfully saved", message.getBody().getMessage());
        when(worldPayProductServiceMock.getValidProducts()).thenReturn(productList);
        List<Product> validProductOffersList = restResource.getValidProducts();

        Assert.assertEquals(3, validProductOffersList.size());
        verify(worldPayProductServiceMock, times(1)).getValidProducts();

    }
    private Product createProduct() {
        Product product = new Product();
        product.setId(1l);
        product.setProductName("abc");
        product.setProductDescription("productDescription1");
        product.setProductUnit("123");
        product.setProductPrice(new BigDecimal(123));
        product.setOfferValidFrom(LocalDateTime.now());
        product.setOfferValidTo(LocalDateTime.now().plusDays(10));
        product.setCancelled(Boolean.FALSE);
        return product;
    }
}