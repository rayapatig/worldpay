package com.uk.worldpay.exercise.services;

import com.uk.worldpay.exercise.model.Product;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Component
public class WorldPayProductCache {
    private final Map<Long, Product> allProducts = new HashMap<>();
    private final AtomicLong id = new AtomicLong(0);
    public WorldPayProductCache() {
        allProducts.put(id.incrementAndGet(), createProduct(id.get(), "prdct1", "productDescription1",
                new BigDecimal(123), "1", LocalDateTime.now(), LocalDateTime.now().plusDays(10), Boolean.FALSE));
        allProducts.put(id.incrementAndGet(), createProduct(id.get(), "prdct1", "productDescription1",
                new BigDecimal(123), "2", LocalDateTime.now(), LocalDateTime.now().plusDays(10), Boolean.FALSE));
        allProducts.put(id.incrementAndGet(), createProduct(id.get(), "prdct1", "productDescription1",
                new BigDecimal(123), "3", LocalDateTime.now(), LocalDateTime.now().plusDays(10), Boolean.FALSE));

    }


    public void saveProduct(Product product) {
        product.setId(id.incrementAndGet());
        allProducts.put(id.get(), product);
    }

    public void updateProduct(long productId,  Product product) {
        if(allProducts.values().contains(product)) {
            allProducts.values().stream()
                    .filter(Objects::nonNull)
                    .filter(f -> !f.getId().equals(productId))
                    .collect(Collectors.toList())
                    .add(product);
        }
    }

    public Map<Long, Product> getAllProducts() {
        return allProducts;
    }

    public List<Product> getCancelledProducts() {
        return allProducts.values().stream()
                .filter(Objects::nonNull)
                .filter(p->p.getCancelled().equals(Boolean.TRUE))
                .collect(Collectors.toList());
    }

    public List<Product> getValidProducts() {
        return allProducts.values().stream()
                .filter(Objects::nonNull)
                .filter(p->p.getOfferValidTo().isAfter(LocalDateTime.now()))
                .collect(Collectors.toList());
    }

    private Product createProduct(long id, String productName, String productDescription, BigDecimal productPrice,
                                  String productUnit, LocalDateTime offerValidFrom, LocalDateTime offerValidTo, Boolean cancelled) {
        Product product = new Product();
        product.setId(id);
        product.setProductName(productName);
        product.setProductDescription(productDescription);
        product.setProductPrice(productPrice);
        product.setProductUnit(productUnit);
        product.setOfferValidFrom(offerValidFrom);
        product.setOfferValidFrom(offerValidTo);
        product.setCancelled(false);

        return product;
    }

    public void cancelProduct(int id) {
        }
}
