package com.uk.worldpay.exercise.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { NoProductOfferException.class})
    protected ResponseEntity<Object> handleNoProductsException(NoProductOfferException ex, WebRequest request) {
        String bodyOfResponse = "There are no products yet!";
        return handleExceptionInternal(ex, bodyOfResponse,
                        new HttpHeaders(), HttpStatus.NO_CONTENT, request);
    }

    @ExceptionHandler(value = { NoSuchProductException.class})
    protected ResponseEntity<Object> handleNoSuchProductsException(NoSuchProductException ex, WebRequest request) {
        String bodyOfResponse = "There is no such product existing!";
        return handleExceptionInternal(ex, bodyOfResponse,
                        new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}
