package com.uk.worldpay.exercise.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Product {
    private Long id;

    /**
     * Name of the product/service what this offer contains. Should be unique.
     */
    private String productName;

    /**
     * User friendly description of the offer. This must be filled out always
     */
    private String productDescription;

    /** Price of the product/service anything in the offer. */
    private BigDecimal productPrice;

    /** Name of the measurement unit if applicable. */
    private String productUnit;

    /** This offer is valid from this date. */
    private LocalDateTime offerValidFrom;

    /** This offer is valid until this LocalDateTime. */
    private LocalDateTime offerValidTo;

    /** True if the offer is cancelled */
    private Boolean cancelled;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductUnit() {
        return productUnit;
    }

    public void setProductUnit(String productUnit) {
        this.productUnit = productUnit;
    }

    public LocalDateTime getOfferValidFrom() {
        return offerValidFrom;
    }

    public void setOfferValidFrom(LocalDateTime offerValidFrom) {
        this.offerValidFrom = offerValidFrom;
    }

    public LocalDateTime getOfferValidTo() {
        return offerValidTo;
    }

    public void setOfferValidTo(LocalDateTime offerValidTo) {
        this.offerValidTo = offerValidTo;
    }

    public Boolean getCancelled() {
        return cancelled;
    }

    public void setCancelled(Boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (id != null ? !id.equals(product.id) : product.id != null) return false;
        if (productName != null ? !productName.equals(product.productName) : product.productName != null) return false;
        if (productDescription != null ? !productDescription.equals(product.productDescription) : product.productDescription != null)
            return false;
        if (productPrice != null ? !productPrice.equals(product.productPrice) : product.productPrice != null)
            return false;
        if (productUnit != null ? !productUnit.equals(product.productUnit) : product.productUnit != null) return false;
        if (offerValidFrom != null ? !offerValidFrom.equals(product.offerValidFrom) : product.offerValidFrom != null)
            return false;
        if (offerValidTo != null ? !offerValidTo.equals(product.offerValidTo) : product.offerValidTo != null)
            return false;
        return cancelled != null ? cancelled.equals(product.cancelled) : product.cancelled == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (productName != null ? productName.hashCode() : 0);
        result = 31 * result + (productDescription != null ? productDescription.hashCode() : 0);
        result = 31 * result + (productPrice != null ? productPrice.hashCode() : 0);
        result = 31 * result + (productUnit != null ? productUnit.hashCode() : 0);
        result = 31 * result + (offerValidFrom != null ? offerValidFrom.hashCode() : 0);
        result = 31 * result + (offerValidTo != null ? offerValidTo.hashCode() : 0);
        result = 31 * result + (cancelled != null ? cancelled.hashCode() : 0);
        return result;
    }
}
