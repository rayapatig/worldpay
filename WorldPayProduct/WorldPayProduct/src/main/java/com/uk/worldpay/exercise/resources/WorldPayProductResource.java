package com.uk.worldpay.exercise.resources;

import com.uk.worldpay.exercise.model.Message;
import com.uk.worldpay.exercise.model.Product;
import com.uk.worldpay.exercise.services.WorldPayProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class WorldPayProductResource {

    public static final String PRODUCT_OFFER_HAS_BEEN_SUCCESSFULLY_ADDED = "product offer has been successfully saved";
    public static final String PRODUCT_OFFER_HAS_BEEN_SUCCESSFULLY_UPDATED = "product offer has been successfully updated";
    @Autowired
    private WorldPayProductService worldPayProductService;

    @RequestMapping(value = "/products", method = RequestMethod.POST)
    public HttpEntity<Message> createProductOffer(@RequestBody Product product) {
        worldPayProductService.saveProductOffer(product);
        return new ResponseEntity<>(new Message(PRODUCT_OFFER_HAS_BEEN_SUCCESSFULLY_ADDED), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/products/{productId}", method = RequestMethod.PUT)
    public HttpEntity<Message> updateProductOffer(@PathVariable("productId") String productId, @RequestBody Product product) {
        worldPayProductService.saveProductOffer(product);
        return new ResponseEntity<>(new Message(PRODUCT_OFFER_HAS_BEEN_SUCCESSFULLY_UPDATED), HttpStatus.OK);
    }

    @RequestMapping(value = "/products/all", method = RequestMethod.GET)
    public  List<Product> getAllProducts() {
        List<Product> products = worldPayProductService.getAllProducts();
        return products;
    }

    @RequestMapping(value = "/products/cancelled", method = RequestMethod.GET)
    public List<Product> getCancelledProducts() {
        List<Product> cancelledProducts = worldPayProductService.getCancelledProducts();
        return cancelledProducts;
    }

    @RequestMapping(value = "/products/valid", method = RequestMethod.GET)
    public List<Product> getValidProducts() {
        List<Product> validProducts = worldPayProductService.getValidProducts();
        return validProducts;
    }
}