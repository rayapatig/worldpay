package com.uk.worldpay.exercise.services;

import com.uk.worldpay.exercise.exception.NoProductOfferException;
import com.uk.worldpay.exercise.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class WorldPayProductService {
    @Autowired
    private WorldPayProductCache worldPayProductCache;

    public void saveProductOffer(Product product) {
        worldPayProductCache.saveProduct(product);
    }

    public void updateProductOffer(long productId, Product product) {
        worldPayProductCache.updateProduct(productId, product);
    }

    public List<Product> getAllProducts() {
        Collection<Product> allProducts = worldPayProductCache.getAllProducts().values();
        if(allProducts.isEmpty()){
            throw new NoProductOfferException();
        }
        return new ArrayList<>(allProducts);
    }

    public List<Product> getCancelledProducts() {
        Collection<Product> cancelledProducts = worldPayProductCache.getCancelledProducts();
        if(cancelledProducts.isEmpty()){
            throw new NoProductOfferException();
        }
        return new ArrayList<>(cancelledProducts);
    }

    public List<Product> getValidProducts() {
        Collection<Product> validProducts = worldPayProductCache.getValidProducts();
        if(validProducts.isEmpty()){
            throw new NoProductOfferException();
        }
        return new ArrayList<>(validProducts);
    }
}
