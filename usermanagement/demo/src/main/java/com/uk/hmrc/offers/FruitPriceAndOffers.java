package com.uk.hmrc.offers;

import java.math.BigDecimal;

public class FruitPriceAndOffers {

	/* The price is in pence */
	public static final int APPLE_PRICE = 60;
	public static final int ORANGE_PRICE = 25;
	public static final String APPLE_OFFER = "Buy one and get one free";
	public static final String ORANGE_OFFER = "3 for price of 2";
}
